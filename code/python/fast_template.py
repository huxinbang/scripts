#!/usr/bin/python
# -*- coding: utf-8 -*-

import json
import sys
import os
import argparse
import glob
import cStringIO

reload(sys)
sys.setdefaultencoding("utf-8")

try: import simplejson as json
except: import json

reload(sys)
sys.setdefaultencoding("utf-8")

def fast_template_parse(source_str, env_vars, script_open_mark="{%",
                        script_close_mark="%}"):
    start = 0
    code_start = 0
    code_end = 0
    open_mark_len = len(script_open_mark)
    close_mark_len = len(script_close_mark)
    output = cStringIO.StringIO()

    code_start = source_str.find(script_open_mark, start)
    while(code_start >= 0):
        code_end = source_str.find(script_close_mark, code_start)
        if (code_end <= code_start):
            print(u"ERROR: " + script_open_mark + " has no match " + script_close_mark + "\n")
            break
        output.write(source_str[start:code_start])
        if source_str[code_start+open_mark_len] == '=':
            code_snip = source_str[code_start+open_mark_len+1:code_end]
            try:
                temp = eval(code_snip,{"output": output}, env_vars)
            except:
                print("ERROR parsing:" + code_snip)
                raise

            if (isinstance(temp, int)):
                output.write(str(temp))
            else:
                output.write(temp)

            start = code_end + close_mark_len
            code_start = source_str.find(script_open_mark, start)
        else:
            code_block = source_str[code_start+open_mark_len:code_end]
            try:
                exec(code_block, {"output": output}, env_vars)
            except:
                print("ERROR parsing:" + code_block)
                raise
            start = code_end + len(script_close_mark)
            
            # skip newline following the close brace '%}'
            if source_str[start] == '\r':
                start += 1
            if source_str[start] == '\n':
                start += 1
            code_start = source_str.find(script_open_mark, start)

    output.write( source_str[start:])
    result = output.getvalue()
    output.close()
    return result

def main():
    try:
        if (len(sys.argv) <= 1) :
            print(
"""
Run command like this:
python fast_template.py /path/to/template_file.txt 'tempalate variables as dictionary'

For example:

echo 'num is {%=num%}
string is {%=str%}
{%
for item in arr:
    output.write("Item:" + item + " ")
%}
' > template_file.txt

python fast_template.py template_file.txt '{"num":1,"str":"String", "arr":["a","b","c"]}'

""")
            return

        template_file = sys.argv[1]

        if (len(sys.argv) > 1):
            env_vars  = json.loads(sys.argv[2])
        else:
            env_vars = {}

        input_file_name = template_file
        input_file = open(input_file_name, "r")
        input_str = input_file.read()

        sys.stdout.write(fast_template_parse(input_str, env_vars))

        return 0
    except ValueError as e:
        print(e)
        return 1


if __name__ == '__main__':
    sys.exit(main())

