#include "stdio.h"

const int i = 1;
#define is_bigendian() ( (*(char*)&i) == 0 )
 
int main(int argc, char const* argv[])
{
    int val;
    char *ptr;
    ptr = (char*) &val;
    val = 0x12345678;
    if (is_bigendian()) {
        printf("big endian: %X.%X.%X.%X\n", ptr[0], ptr[1], ptr[2], ptr[3]);
    } else {
        printf("little endian: %X.%X.%X.%X\n", ptr[3], ptr[2], ptr[1], ptr[0]);
    }
    return 0;
}
