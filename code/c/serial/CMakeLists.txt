add_definitions(-Wall -ggdb)
add_executable(serial src/main.c src/tc_iot_at_driver.c src/ringbuf.c)
target_link_libraries(serial pthread) 

add_executable(ringbuf_test src/ringbuf_test.c src/ringbuf.c)


