#include "tc_iot_at_driver.h"
#include "ringbuf.h"

int g_seriel_fd = -1;
uint8_t ringbuf_area[1024];
ringbuf_t g_serial_buf;

/* at_receive_buff g_at_receive_buff; */

/**
 *@breif 打开串口
 */
static int open_at_dev(const char *Dev)
{
    int	fd = open( Dev, O_RDWR | O_SYNC ); // O_NOCTTY 
    if (-1 == fd)
    { /*设置数据位数*/
        perror("Can't Open Serial Port");
        return -1;
    }
    else
        return fd;

}

/***@brief  设置串口通信速率
 *@param  fd     类型 int  打开串口的文件句柄
 *@param  speed  类型 int  串口速度
 *@return  void*/

int speed_arr[] = { B38400, B19200, B9600, B4800, B2400, B1200, B300,
    B38400, B19200, B9600, B4800, B2400, B1200, B300, };
int name_arr[] = {38400,  19200,  9600,  4800,  2400,  1200,  300,
    38400,  19200,  9600, 4800, 2400, 1200,  300, };
void set_speed(int fd, int speed)
{
    int   i;
    int   status;
    struct termios   Opt;
    tcgetattr(fd, &Opt);
    for ( i= 0;  i < sizeof(speed_arr) / sizeof(int);  i++)
    {
        if  (speed == name_arr[i])
        {
            tcflush(fd, TCIOFLUSH);
            cfsetispeed(&Opt, speed_arr[i]);
            cfsetospeed(&Opt, speed_arr[i]);
            status = tcsetattr(fd, TCSANOW, &Opt);
            if  (status != 0)
                perror("tcsetattr fd1");
            return;
        }
        tcflush(fd,TCIOFLUSH);
    }
}

/**
 *@brief   设置串口数据位，停止位和效验位
 *@param  fd     类型  int  打开的串口文件句柄*
 *@param  databits 类型  int 数据位   取值 为 7 或者8*
 *@param  stopbits 类型  int 停止位   取值为 1 或者2*
 *@param  parity  类型  int  效验类型 取值为N,E,O,,S
 */
int set_Parity(int fd,int databits,int stopbits,int parity)
{
    struct termios options;
    if  ( tcgetattr( fd,&options)  !=  0)
    {
        perror("SetupSerial 1");
        return(FALSE);
    }
    options.c_cflag &= ~CSIZE;
    switch (databits) /*设置数据位数*/
    {
        case 7:
            options.c_cflag |= CS7;
            break;
        case 8:
            options.c_cflag |= CS8;
            break;
        default:
            fprintf(stderr,"Unsupported data size\n");
            return (FALSE);
    }
    switch (parity)
    {
        case 'n':
        case 'N':
            options.c_cflag &= ~PARENB;   /* Clear parity enable */
            options.c_iflag &= ~INPCK;     /* Enable parity checking */
            break;
        case 'o':
        case 'O':
            options.c_cflag |= (PARODD | PARENB);  /* 设置为奇效验*/ 
            options.c_iflag |= INPCK;             /* Disnable parity checking */
            break;
        case 'e':
        case 'E':
            options.c_cflag |= PARENB;     /* Enable parity */
            options.c_cflag &= ~PARODD;   /* 转换为偶效验*/  
            options.c_iflag |= INPCK;       /* Disnable parity checking */
            break;
        case 'S':
        case 's':  /*as no parity*/
            options.c_cflag &= ~PARENB;
            options.c_cflag &= ~CSTOPB;
            break;
        default:
            fprintf(stderr,"Unsupported parity\n");
            return (FALSE);
    }
    /* 设置停止位*/   
    switch (stopbits)
    {
        case 1:
            options.c_cflag &= ~CSTOPB;
            break;
        case 2:
            options.c_cflag |= CSTOPB;
            break;
        default:
            fprintf(stderr,"Unsupported stop bits\n");
            return (FALSE);
    }
    /* Set input parity option */
    if (parity != 'n')
        options.c_iflag |= INPCK;
    options.c_cc[VTIME] = 150; // 15 seconds
    options.c_cc[VMIN] = 0;

    tcflush(fd,TCIFLUSH); /* Update the options and do it NOW */
    if (tcsetattr(fd,TCSANOW,&options) != 0)
    {
        perror("SetupSerial 3");
        return (FALSE);
    }
    return (TRUE);
}

int at_init(tc_iot_at_device * at_device, void * context) {
    int * fd = &(at_device->dev_fd);
    tc_iot_at_dev_context * dev_context = context;
    
    at_device->context = context;
    ringbuf_init(&(at_device->ringbuf), at_device->ringbuf_cache, sizeof(at_device->ringbuf_cache));

    *fd = open_at_dev(dev_context->dev_path);
    if (*fd>0) {
        set_speed(*fd,9600);
    } else {
        printf("Can't Open Serial Port!\n");
        exit(0);
    }

    if (set_Parity(*fd,8,1,'N')== FALSE) {
        printf("Set Parity Error\n");
        exit(1);
    }
    printf("open success fd=%d\n", *fd);
    return 0;
}


int at_send(tc_iot_at_device * p_at_device, const char * data, int len, int timeout_ms){
    AT_TRACE_LOG("%s", data);
    return write(p_at_device->dev_fd, data, len);
}

int at_udp_request(tc_iot_at_device * p_at_device,const char * host, int port, const char * data, int len) {
    char at_command_buff[128];
    int ret = snprintf(at_command_buff, sizeof(at_command_buff), 
            AT_SEND_PACK_BEGIN_FORMAT, 
            host,
            port,
            len);

    if (ret >= sizeof(at_command_buff)) {
        printf("ERROR: %s\n", "at_command_buff overflow.");
        return -1;
    }
    at_send(p_at_device, at_command_buff, ret, AT_SEND_TIMEOUT_MS);
    at_send_binary_as_hex(p_at_device, data, len);
    at_send(p_at_device, AT_EOF, sizeof(AT_EOF)-1, AT_SEND_TIMEOUT_MS);
    return len;
}

int at_close_socket(tc_iot_at_device * p_at_device,int at_socket_fd) {
    char at_command_buff[128];
    int ret = snprintf(at_command_buff, sizeof(at_command_buff), 
            AT_CLOSE_SOCKET_FORMAT, 
            at_socket_fd);

    if (ret >= sizeof(at_command_buff)) {
        printf("ERROR: %s\n", "at_command_buff overflow.");
        return -1;
    }

    at_send(p_at_device, at_command_buff, ret,AT_SEND_TIMEOUT_MS);
    return at_socket_fd;
}

int _at_get_hex_char(char c) {
    if (c >= '0' && c <= '9') {
        return c - '0';
    }
    if (c >= 'a' && c <= 'f') {
        return c - 'a' + 10;
    }
    if (c >= 'A' && c <= 'F') {
        return c - 'A' + 10;
    }
    return 0;
}

int at_udp_receive(tc_iot_at_device * p_at_device,char * data, int len) {
    int i = 0;
    char * pos = NULL;
    int split_count = 0;
    char at_command_buff[128];
    char * socket_no = NULL;
    char * host = NULL;
    char * port = NULL;
    char * resp_len = NULL;
    char * resp_data = NULL;
    char * end_flag = NULL;
    
    int high = 0;
    int low = 0;

    int ret = snprintf(at_command_buff, sizeof(at_command_buff), 
            AT_RECEIVE_PACK_FORMAT, 
            len/2);

    if (ret >= sizeof(at_command_buff)) {
        printf("ERROR: %s\n", "at_command_buff overflow.");
        return -1;
    }

    at_send(p_at_device, at_command_buff, ret,AT_SEND_TIMEOUT_MS);
    // response format:
    // 1,193.112.55.94,8000,17,48656C6C6F2C204920616D20686572652E,0
    ret = at_expect(p_at_device, AT_OK, AT_ERROR, data, len, NET_REQUEST_TIMEOUT_MS);
    if (ret > 0) {
        printf(">data receve success.\n");
        pos = data;
        socket_no = pos;
        for (i = 0; i < len; i++) {
           if (',' == *pos) {
               split_count++;
               *pos = '\0';
               pos++;
               if (1 == split_count) {
                   host = pos;
               } else if (2 == split_count) {
                   port = pos;
               } else if (3 == split_count) {
                   resp_len = pos;
               } else if (4 == split_count) {
                   resp_data = pos;
               } else if (5 == split_count) {
                   end_flag = pos;
                   printf("sock:%s\nhost:%s\nport:%s\nlen :%s\ndata:%s\nflag:%s\n", 
                           socket_no, host, port, resp_len, resp_data, end_flag);
                   break;
               } 
           } else {
               pos++;
           }
        }

        pos = data;

        while(*resp_data) {
            high = _at_get_hex_char(*resp_data);
            resp_data++;
            low = _at_get_hex_char(*resp_data);
            resp_data++;
            *pos = (char)(high << 4) | low;
            pos++;
        }
        *pos = '\0';
        return strlen(data);
    } else {
        return ret;
    }
}

int at_receive(tc_iot_at_device * p_at_device, char * data, int len, int timeout_ms) {
    ringbuf_t * p_ringbuf = &(p_at_device->ringbuf);
    int copy_len = 0;
    int ringbuf_used = 0;
    while ((ringbuf_used=ringbuf_bytes_used(p_ringbuf)) < len) {
        sleep_ms(AT_RECEIVE_TICK_MS);
        timeout_ms -= AT_RECEIVE_TICK_MS;
        if (timeout_ms < 0) {
            break;
        }
    }
    
    if (len < ringbuf_used) {
        copy_len = len;
    } else {
        copy_len = ringbuf_used;
    }
    ringbuf_memcpy_from(data, p_ringbuf, copy_len);
    data[copy_len] = '\0';
    return copy_len;
}

int at_expect(tc_iot_at_device * p_at_device, const char * expect_success, const char * expect_error, char * data, int len, int timeout) {
    int ret = 0;
    int nread = 0;
    int pos = 0;
    const char * expect_pos = NULL;

    while((nread = at_receive(p_at_device, data+pos,len-pos-1 >= 1, AT_RECEIVE_TIMEOUT_MS))>0)
    /* while((nread = at_receive(p_at_device, data+pos,len-pos-1, AT_RECEIVE_TIMEOUT_MS))>0) */
    {
        data[pos+nread]='\0';
        pos += nread;

        expect_pos = strstr(data, expect_success); 
        if (expect_pos != NULL) {
            ret = (expect_pos - data) + strlen(expect_success);
            AT_TRACE_LOG("%s", data);
            return ret;
        }
        
        if (expect_error) {
            expect_pos = strstr(data, expect_error); 
            if (expect_pos != NULL) {
                ret = (expect_pos - data) + strlen(expect_error);
                AT_TRACE_LOG("%s", data);
                return -ret;
            }
        }
    }
    printf("%s\n", ">No data received.");
    return 0;
}

int at_send_binary_as_hex(tc_iot_at_device * p_at_device,const char * data, int len) {
    int i = 0;
    unsigned char high, low;
    char buff[3];
    int temp;
    static const char map_byte_to_hex[16] = {
        '0', '1', '2', '3', '4', '5', '6', '7',
        '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
    };

    buff[2] = '\0';

    for(i = 0;i < len; i++){
        temp = data[i];
        high = (temp >> 4) & 0xF;
        low  = temp & 0xF;
        buff[0] = map_byte_to_hex[high];
        buff[1] = map_byte_to_hex[low];
        at_send(p_at_device, buff, 2,AT_SEND_TIMEOUT_MS);
    }
    return 2*len;
}

int at_destroy(tc_iot_at_device * p_at_device){
    int *fd = &p_at_device->dev_fd;
    close(*fd);
    *fd = -1;
    return 0;
}

