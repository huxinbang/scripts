#ifndef INCLUDED_RINGBUF_H
#define INCLUDED_RINGBUF_H

#include <stddef.h>
#include <sys/types.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/param.h>
#include <assert.h>

typedef struct _ringbuf_t
{
    uint8_t *buf;
    uint8_t *head;
    uint8_t *tail;
    int32_t size;
} ringbuf_t;


int ringbuf_init(ringbuf_t * rb, uint8_t * buffer, int buffer_len);
int32_t ringbuf_buffer_size(const ringbuf_t *rb);
void ringbuf_free(ringbuf_t *rb);
void ringbuf_reset(ringbuf_t *rb);

int32_t ringbuf_capacity(const ringbuf_t *rb);
int32_t ringbuf_bytes_free(const ringbuf_t *rb);
int32_t ringbuf_bytes_used(const ringbuf_t *rb);
int ringbuf_is_full(const ringbuf_t *rb);
int ringbuf_is_empty(const ringbuf_t *rb);
const void * ringbuf_tail(const ringbuf_t *rb);
const void * ringbuf_head(const ringbuf_t *rb);
int32_t ringbuf_findchr(const ringbuf_t *rb, int c, int32_t offset);
int32_t ringbuf_memset(ringbuf_t * dst, int c, int32_t len);
void * ringbuf_memcpy_into(ringbuf_t * dst, const void *src, int32_t count);

int32_t ringbuf_read(int fd, ringbuf_t * rb, int32_t count);
void * ringbuf_memcpy_from(void *dst, ringbuf_t * src, int32_t count);
int32_t ringbuf_write(int fd, ringbuf_t * rb, int32_t count);
void * ringbuf_copy(ringbuf_t * dst, ringbuf_t * src, int32_t count);

#endif /* INCLUDED_RINGBUF_H */
