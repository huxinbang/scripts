#ifndef TC_IOT_AT_DRIVER_H
#define TC_IOT_AT_DRIVER_H

#include     <stdio.h>      /*标准输入输出定义*/
#include     <stdlib.h>     /*标准函数库定义*/
#include     <unistd.h>     /*Unix标准函数定义*/
#include     <sys/types.h>  /**/
#include     <sys/stat.h>   /**/
#include     <fcntl.h>      /*文件控制定义*/
#include     <termios.h>    /*PPSIX终端控制定义*/
#include     <errno.h>      /*错误号定义*/
#include     <string.h>      /*错误号定义*/
#include     "ringbuf.h"

#define FALSE 0
#define TRUE  1

#define AT_EOF    "\r\n"
#define AT_OK     "OK\r\n"
#define AT_ERROR  "ERROR\r\n"

#define AT_RST "AT+NRB" AT_EOF
#define AT_CSQ "AT+CSQ" AT_EOF
#define AT_UDP_SOCKET "AT+NSOCR=DGRAM,17,0,1" AT_EOF
#define AT_CLOSE_SOCKET_FORMAT "AT+NSOCL=%d" AT_EOF

#define AT_SEND_PACK_BEGIN_FORMAT  "AT+NSOST=1,%s,%d,%d,"
#define AT_SEND_PACK_END    AT_EOF

#define AT_RECEIVE_PACK_FORMAT "AT+NSORF=1,%d" AT_EOF

#define AT_PACK_ARRIVE_INDICATOR  "+NSONMI:"

#define AT_TRACE_LOG printf

typedef struct _tc_iot_at_dev_context {
    const char * dev_path;
} tc_iot_at_dev_context;

typedef struct _tc_iot_at_device {
    int dev_fd;
    void * context;
    ringbuf_t ringbuf;
    uint8_t ringbuf_cache[1024];
} tc_iot_at_device;

int at_init(tc_iot_at_device * p_at_device, void * context);
int at_send(tc_iot_at_device * p_at_device, const char * data, int len,int timeout_ms);
int at_receive(tc_iot_at_device * p_at_device,char * data, int max_len,int timeout_ms);

int at_expect(tc_iot_at_device * p_at_device,const char * expect_success, const char * expect_error, char * data, int len, int timeout_ms);

int at_send_binary_as_hex(tc_iot_at_device * p_at_device,const char * data, int len);
int at_destroy(tc_iot_at_device * at_device);
int at_close_socket(tc_iot_at_device * p_at_device,int at_socket_fd);
int at_udp_request(tc_iot_at_device * p_at_device,const char * host, int port, const char * data, int len);
int at_udp_receive(tc_iot_at_device * p_at_device,char * data, int len);

#define sleep_ms(x)   usleep((x)*1000)

#define NET_REQUEST_TIMEOUT_MS   (10*1000)
#define AT_TIMEOUT_MS            2000
#define AT_SEND_TIMEOUT_MS       AT_TIMEOUT_MS
#define AT_RECEIVE_TIMEOUT_MS    AT_TIMEOUT_MS
#define AT_RESET_TIMEOUT_MS      (20*1000)
#define AT_RECEIVE_TICK_MS       100


#endif /* end of include guard */
