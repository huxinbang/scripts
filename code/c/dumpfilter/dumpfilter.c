#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <regex.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>

#define BUFFER_SIZE    65535

#if __APPLE__
#endif

int dumpfilter( FILE*);
char ascii_to_char(char ascii);
int parse_tcp_buffer(const char * buffer, size_t size);
int parse_udp_buffer(const char * buffer, size_t size);


regex_t regTitle;
regex_t regData;

int main(int argc, char *argv[])
{
    int i;
    FILE *f;
    int ret;
    int    status;
    char ebuf[128];
    char * title_pattern = "[0-9]+:[0-9]+:[0-9]+";
    char * line_pattern = ".*0x[0-9]*:  ";

    ret = regcomp(&regTitle, title_pattern, REG_EXTENDED|REG_NOSUB);

    if (ret != 0) {
        regerror(ret, &regTitle, ebuf, sizeof(ebuf));
        fprintf(stderr, "%s: pattern '%s' \n",ebuf, title_pattern);
        return 1;
    }
    ret = regcomp(&regData, line_pattern, REG_EXTENDED|REG_NOSUB);
    if (ret != 0) {
        regerror(ret, &regData, ebuf, sizeof(ebuf));
        fprintf(stderr, "%s: pattern '%s' \n",ebuf, line_pattern);
        return 1;
    }

    if (argc == 1) {
        dumpfilter(stdin);
    } else {
        for (i = 1; i < argc; i++) {
            f = fopen(argv[i], "r");
            if (f == NULL) {
                printf("can't open %s:", argv[i]);
                continue;
            }
            dumpfilter(f);

            fclose(f);
        }
    }

    /* free regex */
    regfree(&regTitle);
    regfree(&regData);
    /* if (status != 0) { */
    /* return(0);      [> Report error. <] */
    /* } */
    return 0;
}

int dumpfilter(FILE *f)
{
    const int hexstart = 10;
    const int hexcount = 40;
    const int hexlen   = 32;
    int n;
    static char buf[BUFFER_SIZE];
    static char buf_new[BUFFER_SIZE];
    static int cpy_size = 0;

    while (fgets(buf, sizeof buf, f) != NULL) {
        n = strlen(buf);
        if (n > 0 && buf[n-1] == '\n')
        {
            buf[n-1] = '\0';
        }

        int status;
        status = regexec(&regTitle, buf, (size_t) 0, NULL, 0);
        if (status == 0)
        {
            if (cpy_size > 0)
            {
                // TODO process data;
                buf_new[cpy_size] = 0;
                parse_udp_buffer(buf_new, cpy_size);
            }

            // re-intial for next package
            cpy_size = 0;
            printf("T: %s\n", buf);
            continue;
        }

        status = regexec(&regData, buf, (size_t) 0, NULL, 0);
        if (status == 0)
        {
            int i = 0;
            for ( i = 0; i < hexcount; i ++)
            {
                if ((buf[hexstart + i] == ' ')
                        && (buf[hexstart + i + 1] == ' '))
                {
                    /* printf("break\n"); */
                    break;;
                }

                if (buf[hexstart + i] == ' ')
                {
                    /* printf("continue\n"); */
                    continue;
                }

                buf_new[cpy_size] = 
                    ascii_to_char(buf[hexstart + i]) * 0x10 +
                    ascii_to_char(buf[hexstart + i + 1]) ;

                /* printf("char: %2x.%2x:%02x\n",  */
                        /* buf[hexstart + i], */
                        /* buf[hexstart + 1 + i], */
                        /* (unsigned char) buf_new[cpy_size] */
                      /* ); */

                i++;
                cpy_size++;
            }
            buf_new[cpy_size] = 0;
            continue;
        }

    }
    /* printf("%s.\n", "mean package end"); */
    if (cpy_size > 0)
    {
        // TODO process data;
        buf_new[cpy_size] = 0;
        parse_udp_buffer(buf_new, cpy_size);
    }
    return 0;
}

char ascii_to_char(char ascii)
{
    if (ascii >= '0' && ascii <= '9')
    {
        return ascii - '0';
    }

    if (ascii >= 'a' && ascii <= 'f')
    {
        return ascii - 'a' + 0xa;
    }

    if (ascii >= 'A' && ascii <= 'F')
    {
        return ascii - 'A' + 0xa;
    }

    return -1;
}


int parse_udp_buffer(const char * buffer, size_t size)
{
#if __APPLE__

    if (size < sizeof(struct ip) + sizeof(struct udphdr))
    {
        printf("%s\n", "invalid udp dump");
        return -1;
    }

    struct ip * pip  = (struct ip *)buffer;
    struct udphdr * udp  = (struct udphdr *)(buffer + sizeof *pip);

    ssize_t data_len  =  ntohs(udp->uh_ulen) - sizeof (*udp);
    printf("%d = %d - %d\n", (unsigned)data_len, (unsigned)ntohs(udp->uh_ulen),  (unsigned)sizeof(*udp));
    if (data_len < 0)
    {
        printf("%s\n", "invalid udp dump");
        return -1;
    }

    printf("src_port: %d, dest_port: %d, len: %zu\n", 
            ntohs(udp->uh_sport), ntohs(udp->uh_dport), 
            data_len);

    /* return 0; */

    char * data = (char *)(buffer + sizeof *pip + sizeof *udp);
    int i;
    for (i = 0; i < data_len; i++) {
        if (i%16 == 0) {
            printf("data-0x%04x: ", i/16);
        }
        printf("%02x ", (uint8_t)data[i]);
        if (i%16 == 15) {
            printf("\n");
        }
    }

    printf("\n");

    /* printf("data:%s\n", data); */
#endif
    /* struct iphdr * pip  = (struct iphdr *)buffer; */
    /* struct udphdr * udp  = (struct udphdr *)(buffer + sizeof *pip); */

    /* printf("src_port: %d, dest_port: %d, len: %d\n",  */
    /* ntohs(udp->source), ntohs(udp->dest),  */
    /* ntohs(udp->len) - sizeof (*udp)); */

    /* char * data = (char *)(buffer + sizeof *ip + sizeof *udp); */

    /* printf("data:%s\n", data); */

    return 0;
}
