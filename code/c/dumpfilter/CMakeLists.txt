cmake_minimum_required(VERSION 2.8)

project(dumpfilter)

add_definitions("-DDEBUG -g")

add_executable(dumpfilter dumpfilter.c)

