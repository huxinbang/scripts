#include <stdio.h>
#include <unistd.h>

#define ANSI_COLOR_BLACK   "\x1b[30m"
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_WHITE   "\x1b[37m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#define ANSI_RIS           "\x1b[2J"

int main (int argc, char const *argv[]) {
    int i;

    for (i = 0; i < 100; i++ ){
        printf(ANSI_RIS);
        printf(ANSI_COLOR_BLACK   "This text is BLACK!"    ANSI_COLOR_RESET "\n");
        sleep(1);
        printf(ANSI_COLOR_RED     "This text is RED!"     ANSI_COLOR_RESET "\n");
        sleep(1);
        printf(ANSI_COLOR_GREEN   "This text is GREEN!"   ANSI_COLOR_RESET "\n");
        sleep(1);
        printf(ANSI_COLOR_YELLOW  "This text is YELLOW!"  ANSI_COLOR_RESET "\n");
        sleep(1);
        printf(ANSI_COLOR_BLUE    "This text is BLUE!"    ANSI_COLOR_RESET "\n");
        sleep(1);
        printf(ANSI_COLOR_MAGENTA "This text is MAGENTA!" ANSI_COLOR_RESET "\n");
        sleep(1);
        printf(ANSI_COLOR_CYAN    "This text is CYAN!"    ANSI_COLOR_RESET "\n");
        sleep(1);
        printf(ANSI_COLOR_WHITE   "This text is WHITE!"    ANSI_COLOR_RESET "\n");
    }
    

    return 0;
}
