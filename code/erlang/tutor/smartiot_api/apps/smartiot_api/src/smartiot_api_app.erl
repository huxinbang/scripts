%%%-------------------------------------------------------------------
%% @doc smartiot_api public API
%% @end
%%%-------------------------------------------------------------------

-module(smartiot_api_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%%====================================================================
%% API
%%====================================================================

start(_StartType, _StartArgs) ->
    {ok, Pid} = smartiot_api_sup:start_link(),
    Routes = [ {
                '_',
                [
                 {"/", smartiot_api_root, []},
                 {"/query", smartiot_api_rest, [{op, query}]},
                 {"/log", smartiot_api_rest, [{op, log}]}
                ]
               } ],
    Dispatch = cowboy_router:compile(Routes),

    TransOpts = [ {ip, {0,0,0,0}}, {port, 3000} ],
    ProtoOpts = #{env => #{dispatch => Dispatch}},

    {ok, _} = cowboy:start_clear(chicken_poo_poo, TransOpts, ProtoOpts),
    smartiot_api_sup:start_link(),
    {ok, Pid}.

%%--------------------------------------------------------------------
stop(_State) ->
    ok.

%%====================================================================
%% Internal functions
%%====================================================================
