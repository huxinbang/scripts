-module(smartiot_api_rest).
-export([
         init/2,
         allowed_methods/2,
         content_types_provided/2,
         content_types_accepted/2
        ]).

-export([
         to_json/2,
         from_json/2
        ]).

-record(state, {op, response}).

%%%-------------------------------------------------------------------
%%% Cowby ReST built-in callbacks
%%%-------------------------------------------------------------------

init(Req, Opts) ->
    [{op, Op} | _] = Opts,
    State = #state{op=Op, response=none},
    {cowboy_rest, Req, State}.

allowed_methods(Req, State) ->
    Value = [<<"GET">>, <<"POST">>, <<"HEAD">>],
    {Value, Req, State}.

content_types_accepted(Req, State) ->
    Value = [
             {<<"application/x-www-form-urlencoded">>, from_json}
            ],
    {Value, Req, State}.

content_types_provided(Req, State) ->
    Value = [
             {{ <<"application">>, <<"json">>, '*'}, to_json}
            ],
    {Value, Req, State}.

to_json(Req, #state{op=Op} = State) ->
                                                %io:fwrite("~n-----~n(to_json) Req: ~p~n-----~n", [Req]),
    Result = case Op of
                 log ->
                     to_json_log(Req, State);
                 query ->
                     to_json_query(Req, State);
                 help ->
                     to_json_help(Req, State)
             end,
                                                %io:fwrite("~n-----~nResult: ~p~n-----~n", [Result]),
    Result.

from_json(Req, #state{op=Op} = State) ->
                                                %Method = cowboy_req:method(Req),
                                                %io:fwrite("(from_json) Method: ~p~n", [Method]),
    Result = case Op of
                 query ->
                                                %io:fwrite("(from_json) create~n", []),
                     Reply = cowboy_req:read_urlencoded_body(Req),
                                                %io:fwrite("(from_json) Reply: ~p~n", [Reply]),
                     {ok, [{<<"content">>, Content}], Req1} = Reply,
                     Body = {[]},
                     Body1 = jiffy:encode(Body),
                                                %io:fwrite("*** (from_json) Body1: ~p~n", [Body1]),
                     {{true, Body1}, Req1, State};
                 log ->
                                                %io:fwrite("(from_json) update~n", []),
                     %% RecordId = cowboy_req:binding(doc_id, Req),
                     Reply = cowboy_req:read_urlencoded_body(Req),
                                                %io:fwrite("(from_json) Reply: ~p~n", [Reply]),
                     {ok, [{<<"content">>, Content}], Req1} = Reply;
                                                %io:fwrite("(from_json) PUT RecordId: ~p  Content: ~p~n",
                                                %          [RecordId, Content]),
                 true ->
                     Reply = cowboy_req:read_urlencoded_body(Req)
             end,
    Result.

%%%-------------------------------------------------------------------
%%% Private functions
%%%-------------------------------------------------------------------

to_json_query(Req, State) ->
    Body = {[
             {returnCode,0},
             {returnMessage,<<"OK">>},
             %% {productId,ProductId},
             {data,{[
                     {mqtt_host,<<"localhost">>},
                     {mqtt_ip,<<"127.0.0.1">>},
                     {http_host,<<"localhost">>},
                     {http_ip,<<"127.0.0.1">>},
                     {log_server_host,<<"localhost">>},
                     {log_server_ip,<<"127.0.0.1">>}
                    ]}
             }
            ]},
    Body1 = jiffy:encode(Body),
    {Body1, Req, State}.

to_json_log(Req, State) ->
    Body = {[
             {returnCode,0},
             {returnMessage,<<"OK">>}
            ]},
    Body1 = jiffy:encode(Body),
    {Body1, Req, State}.


to_json_help(Req, State) ->
    Body = {[
             {<<"version">>, <<"4">>},
             {<<"app">>, <<"smartiot api">>},
             {<<"/query">>, <<"Query mqtt server and log server ...">>},
             {<<"/log">>, <<"Upload log.">>},
             {<<"/help">>, <<"print this help">>}
            ]},
    {jiffy:encode(Body), Req, State}.