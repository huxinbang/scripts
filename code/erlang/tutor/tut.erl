-module(tut).
-export([
         double/1, 
         triple/1,
         add/2,
         test_if/1,
         test_list/1,
         reverse/1,
         list_max/1
        ]).

double(X) ->
    2 * X.

triple(X) ->
    3 * X.

add(A,B) ->
    A+B.

test_if(A) ->
    if
        A == 1 ->
            io:format("A equals 1 ~n", []),
            a_eq_1;
        A == 2 ->
            io:format("A equals 2 ~n", []),
            a_eq_2;
        A > 2 ->
            io:format("A larger than 2 ~n", []),
            a_l_2
    end.


test_list([]) ->
    io:format("~n");
test_list(L) ->
    [H|T] = L,
    io:format("~w|", [H]),test_list(T).

reverse(List) ->
    reverse(List, []).

reverse([Head | Rest], Reversed_List) ->
    io:format("~w ~w ~w~n", [Head, Rest, Reversed_List]),
    reverse(Rest, [Head | Reversed_List]);
reverse([], Reversed_List) ->
    Reversed_List.

list_max([Head | Rest]) ->
    list_max(Rest, Head).

list_max([], Max) ->
    Max;
list_max([Head | Rest], Max) when Head > Max ->
    list_max(Rest, Head);
list_max([Head | Rest], Max) when Head =< Max ->
    list_max(Rest, Max).
%% list_max([Head | Rest], Max) ->
%%     if Head > Max ->
%%             list_max(Rest, Head);
%%        true ->
%%             list_max(Rest, Max)
%%     end.

